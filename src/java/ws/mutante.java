/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import beans.Mutante;
import beans.Usuario;
import dao.MutanteDao;
import dao.UsuarioDao;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author user
 */
@Path("mutante")
public class mutante {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of mutante
     */
    public mutante() {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/novo-mutante/{nome}/{habilidade1}/{habilidade2}/{habilidade3}/")
    public String getAutenticacao(@PathParam("nome") String nome,
            @PathParam("habilidade1") String habilidade1,@PathParam("habilidade2") String habilidade2,
            @PathParam("habilidade3") String habilidade3) {
        //TODO return proper representation object
        Mutante mutante = new Mutante();
        mutante.setNome(nome);
        mutante.setHabilidade1(habilidade1);
        mutante.setHabilidade2(habilidade2);
        mutante.setHabilidade3(habilidade3);
        
        MutanteDao mutanteDao = new MutanteDao();       
        int i = mutanteDao.addMutant(mutante);
        if (i==0){
            return "true";
        }else
            return "false";
    }
    
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/login/{usuario}/{senha}")
    public String getAutenticacao(@PathParam("usuario") String usuario,
            @PathParam("senha") String senha) {
        //TODO return proper representation object
        UsuarioDao dao = new UsuarioDao();
        Usuario u = new Usuario();
        u = dao.getAuthenticateString(usuario, senha);
        if(u != null)
            return "true";
        return "false";
        
    }
    //http://localhost:39473/AppMutantes/webresources/mutanteResource/login/Lia/ahbas
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/mutante/{nome}")
    public String Mutante(@PathParam("nome") String nome) {
        //TODO return proper representation object
        Mutante m = new Mutante();
        m.setNome(nome);
        m.setId(1);
        
        return m.getNome();
    }
    //http://localhost:39473/AppMutantes/webresources/mutanteResource/mutante/John

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getmutante/{nome}")
    public String getMutante(@PathParam("nome") String nome) {
        //TODO return proper representation object
        Mutante m = new Mutante();
        m = new MutanteDao().getMutant(nome);
        
        return m.getNome();
    }
}
