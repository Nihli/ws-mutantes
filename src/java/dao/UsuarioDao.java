/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import beans.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UsuarioDao {
    
    Connection con = new ConnectionFactory().getConnection();

    public Usuario getAuthenticateString (String name, String password) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement("SELECT nome FROM usuario WHERE nome = ? AND senha = ? ");
            ps.setString(1, name);
            ps.setString(2, password);
            rs = ps.executeQuery();
            if (rs.next()) {
                Usuario u = new Usuario();
                u.setNome(rs.getString("nome"));
             //   u.setSenha(rs.getString("senha"));
                return u;
            }
                return null;
        } catch (SQLException e) {
            e.printStackTrace();
                    return null;
        }
    }
}
