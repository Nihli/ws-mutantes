/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;


import beans.Mutante;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MutanteDao {
    
    Connection con = new ConnectionFactory().getConnection();
	
	public Mutante getMutant(String name) {
        //Configurações para mandar a Query ao BD;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement("SELECT NOME, HABILIDADE1 FROM mutante WHERE NOME = ? ");
            //faz com que o "?" na query se torne o argumento que o método recebeu;
            ps.setString(1, name);
            rs = ps.executeQuery();
            //A Query pode retornar mais de um resultado, portanto armazenamos eles em uma lista;
            List<Mutante> list = new ArrayList<>();
            while (rs.next()) {
                Mutante m = new Mutante();
                m.setNome(rs.getString("NOME"));
                list.add(m);
            }
            //Se a lista não estiver vazia é porque retornou ao menos um resultado;
            //Aqui retornamos o primeiro;
            if(!list.isEmpty())
                return list.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    
        public int addMutant(Mutante mutante) {
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("INSERT INTO mutante (nome, habilidade1, habilidade2, habilidade3)"
                    + " VALUES(?,?,?,?)");
            st.setString(1, mutante.getNome());
            st.setString(2, mutante.getHabilidade1());
            st.setString(3, mutante.getHabilidade2());
            st.setString(4, mutante.getHabilidade3());
            st.executeUpdate();
            return 0;
        } catch (SQLException ex) {
            Logger.getLogger(MutanteDao.class.getName()).log(Level.SEVERE, null, ex);
            return 1;
        }
    }    
        
        
}
